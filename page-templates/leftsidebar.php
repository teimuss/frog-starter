<?php
/**
 * Template Name: Left Sidebar Page
 *
 * Description: Page with sidebar on the left
 *
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeName themeVersion
 */

get_header(); ?>
<?php get_sidebar(); ?>
	<div id="primary" class="content-area pure-u-2-3 two-thirds column" >
		<div id="content" class="site-content" role="main">
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer(); ?>

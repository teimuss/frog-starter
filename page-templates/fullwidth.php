<?php
/**
 * Template Name: Full-width Page Template, No Sidebar
 *
 * Description: Page without sidebar
 *
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeName themeVersion
 */

get_header(); ?>

	<div id="primary" class="content-area pure-u-1 twelve columns" >
		<div id="content" class="site-content" role="main">
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			 <?php themeTextDomain_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_footer(); ?>

<?php
/**
 * themeName functions and definitions.
 * Starter theme based on frostarter wordpress theme
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeVersion
 */

do_action( 'themeHandle_init' );

/*
* Utility class
*/
require_once( 'inc/class/class-utils.php' );
Util::activate( 'add_markup_to_menu' );


/*
* Initial theme setup and constants
*/
require_once( 'inc/init.php' );

/*
* Sidebar + widget registration
*/
require_once( 'inc/widgets.php' );


/*
 * Helper functions
 */
require_once( 'inc/config.php' );
require_once( 'inc/scripts.php' );


/*
* Composer libraries
*/
require_once( 'inc/vendor/autoload.php' );

/*
* Debug dump
*/

/*
 * require_once( 'inc/debug.php' ); // Move this to frog-debug plugin.
 * R_Debug::list_live_hooks(); // List live hooks.
 */

/*
* Custom post types class
*/
require_once( 'inc/cpt.php' );


/*
* Pagination class - includes ajax functionality
*/

/*
 * require_once( 'inc/class/class-pagination.php' );
 */

/*
* Image class
*/

/*
 * require_once( 'inc/class/class-images.php' );
 */

/*
* Template classes
*/

/*
 * require_once( 'inc/class/class-template-facade.php' );
 */

/*
* Customization options
*/

/*
 * require_once( 'inc/customizer/kirki.php' );  // Landing page option
 */

/*
* Custom roles
*/

/*
 * require_once( 'inc/roles.php' );
 */

/*
* Custom functions
*/
require_once( 'inc/custom.php' );

/*
 * require_once( 'inc/shortcodes.php' );
 * require_once( 'inc/tinymce.php' );
 */

/*
* REST API
*/

/*
 * require_once( 'inc/class/class-rest-api.php' );
 * require_once( 'inc/rest-api.php' );
 */


/*
* Advanced Custom Fields functions
*/

/*
 * require_once( 'inc/acf.php' );
 */

/*
* Admin backend functions
*/

/*
 * require_once( 'inc/admin.php' )
 */


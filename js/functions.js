/**
* Functionality specific to themeName.
*
* Provides helper functions to enhance the theme experience.
*/

( function( $ ) {
	'use strict;';

	$(document).ready(function(){
		$('sf-menu').superfish();
	});

} )( jQuery );

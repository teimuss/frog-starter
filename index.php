<?php
/**
 * The main template file.
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeName themeVersion
 */

get_header(); ?>

	<div id="primary" class="content-area" >
		<div id="content" class="site-content" role="main">

		<!-- remove this -->
		<a href="<?php echo get_template_directory_uri(); ?>/README.md" style="font-size:2em; color:red; display:inline-block; padding: 1em 0;">README</a>

		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

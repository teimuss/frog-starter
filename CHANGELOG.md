## 1.0.0 (2016-07-17)
 * Complete webpack migration

## 0.7.1 (2015-09-17)
 * Minor bug fixes

## 0.7.0 (2015-09-17)
* Add Pagination Class
* Add Util Class
* Add ImageFill Class
* Remove util file
* Minor bug fixes

## 0.6.1 (2015-08-26)
* Add html5shiv to bower.json.
* Fix theme name and version text placeholders.

## 0.6.0 (2015-08-24)
* Prepare theme for yeoman generator

## 0.5.0 (2015-06-14)
* Removed classes names for agnostic free framework structure.
* Refined grunt generation file.
* Better documentation

## 0.4.0 (2015-05-27)
* Add Sass to the workflow

## 0.3.0 (2015-05-26)
* New feature - Add Grunt to the workflow

## 0.2.0 (2015-05-26)
* Add css breakpoints to pure and skeleton frameworks

## 0.1.0 (2013-03-01)
* First commit

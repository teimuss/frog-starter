module.exports = function(grunt) {
'use strict';

  var pkg = grunt.file.readJSON('package.json');

  grunt.loadNpmTasks('grunt-composer');

  grunt.initConfig({
    yuidoc: {
      compile: {
        name: pkg.name,
        description: pkg.description,
        version: pkg.version,
        url: '#',
        logo: '../images/logo_void.png',
        options: {
          paths: [ '.'],
          ignorePaths: ['node_modules', 'tools', 'yuidoc-theme', 'build'],
          outdir: 'docs/',
          extension: '.php',
          parseOnly: false,
          themedir: 'yuidoc-theme/blue',
          // helpers: [ 'yuidoc-theme/blue/helpers/helpers.js' ]
        },
      }
    },
    fontello: {
      build: {
        options: {
          config  : 'config.json',
          fonts   : 'fonts/fontello/font',
          fontelloSettings
          force   : true
        }
      }
    },
    composer : {
        options : {
            cwd: 'inc/'
        }
    },
    notify: {
      build: {
        options: {
          title: 'Task Complete',  // optional
          message: 'Production build complete', //required
        }
      },
      watch: {
        options: {
          title: 'Watch/Sass Task',  // optional
          message: 'File change', //required
        }
      }
    },
    changelog: {
      options: {
        repository: 'https://bitbucket.org/teimuss/themeTextDomain.git'
      }
    },
    bump: {
      options: {
        files: [
          'package.json',
          'bower.json',
          'style.css'
        ],
        updateConfigs: [],
        commit: false,
        commitMessage: 'Release v%VERSION%',
        commitFiles: ['package.json'],
        createTag: true,
        tagName: 'v%VERSION%',
        tagMessage: 'version: %VERSION%',
        push: false,
        pushTo: 'master',
        gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d'
      }
    },
    uglify: {
      dist: {
        files: {
          'build/scripts.min.js': [
            'js/plugins/*.js',
            'js/_*.js',
            'js/*.js',
            'inc/js/*.js'
          ],
          'inc/customizer.min.js' : 'inc/customizer.js',
          'inc/customizer.preview.min.js' : 'inc/customizer.preview.js'
        }
      },
    },
    cssmin: {
      main: {
        files: {
          'build/main.min.css': [
            cssminConfig
            'css/main.css'
          ]
        }
      },
      ie: {
        files: {
          'build/ie.min.css': [
            'css/ie/*.css'
          ]
        }
      }
    },
    concurrent: {
      server: {
        // tasks: ['watch', 'compass:compile'], //compass
        tasks: ['watch'], //libsass or sass
        options: {
            logConcurrentOutput: true
        }
      }
    },
    compass: {
      compile: {
        options: {
          watch: true,
          sassDir: 'sass',
          cssDir: 'css',
          outputStyle: 'expanded', //nested, expanded, compact, compressed
        }
      }
    },
    sass: {
        options: {
            sourceMap: true,
            style: 'expanded'
        },
        compile: {
            files: [{
              expand: true,
              cwd: 'sass',
              src: ['*.scss'],
              dest: 'css',
              ext: '.css'
            }]
        }
    },
    // sass: { //grunt-contrib-sass
    //   compile: {
    //     options: {
    //       style: 'expanded'
    //     },
    //     files: [{
    //       expand: true,
    //       cwd: 'sass',
    //       src: ['*.scss'],
    //       dest: 'css',
    //       ext: '.css'
    //     }]
    //   }
    //},
    watch: {
      options: {
        livereload: true,
      },
      js: {
        files: [
          'js/*.js',
          '!js/*.min.js',
          'inc/*.js',
          '!inc/*.min.js',
        ],
        tasks: ['notify:watch']
      },
      css:{
        files: ['css/**/*.css'],
        livereload: true,
        tasks: ['notify:watch']
      },
      build:{
        files: ['build/*']
      },
      sass:{ //libsass
        files: ['sass/**/*.scss'],
        tasks:['sass:compile', 'notify:watch']
      },
      livereload: {
        files: ['!bower_components/**', '!sass/**','!build/**', '!node_modules/**', '*.html', '*.php', 'images/**/*.{png,jpg,jpeg,gif,webp,svg}'],
        tasks: ['notify:watch']
      }
    },
    clean: {
      dist: [
        'build/main.min.css',
        'build/scripts.min.js'
      ]
    },
    availabletasks: {
      user: {
        options: {
          showTasks: ['user'],
          descriptions: {
            'availabletasks' : 'A really nice task list helper.',
            'build' : 'Prepares files to be deployed [Minify, Concat, etc]',
            'dev' : 'Watch js files for changes than uglifies',
            'default' : 'List user defined tasks',
            'list' : 'List all available tasks',
          }
        }
      },
      all: {
        options: {
          descriptions: {
            'availabletasks' : 'A really nice task list helper.',
            'build' : 'Prepares files to be deployed [Minify, Concat, etc]',
            'dev' : 'Watch js files for changes than uglifies',
            'default' : 'List user defined tasks',
            'list' : 'List all available tasks',
          }
        }
      }
    }
  });

  // Load tasks
  // grunt.loadTasks('tasks');
  grunt.loadNpmTasks('grunt-concurrent');
  // grunt.loadNpmTasks('grunt-contrib-compass'); //when running compass
  // grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-yuidoc');
  grunt.loadNpmTasks('grunt-bump');
  grunt.loadNpmTasks('grunt-conventional-changelog');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-fontello');


  // Register tasks
  grunt.registerTask('default', ['availabletasks:user']);
  grunt.registerTask('list', ['availabletasks:all']);

  grunt.registerTask('build', [
    'clean',
    'newer:uglify',
    'newer:cssmin',
    'notify:build',
    'changelog',
  ]);

  grunt.registerTask('dev', ['concurrent']);

  grunt.task.registerTask('setup', '', function() {

    setupTaskConfig

    grunt.file.copy('bower_components/html5shiv/dist/html5shiv.js','inc/js/html5shiv.js');
    grunt.file.copy('bower_components/superfish/dist/js/superfish.js','js/plugins/superfish.js');
    grunt.file.copy('bower_components/superfish/dist/css/superfish.css','css/plugins/superfish.css');

    grunt.task.run([
      'composer:install',
      'fontello'
    ]);

  });

};

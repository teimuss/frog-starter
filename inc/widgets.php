<?php

/**
 * Registers two widget areas.
 *
 * @method themeHandle_widgets_init
 * @private
 * @type action
 * @since themeName themeVersion
 *
 * @uses widgets_init action
 * @uses register_sidebar
 */
function themeHandle_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'themeTextDomain' ),
		'id'            => 'sidebar-main',
		'description'   => __( 'Appears on posts and pages', 'themeTextDomain' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'themeTextDomain' ),
		'id'            => 'sidebar-footer',
		'description'   => __( 'Appears in the footer section of the site', 'themeTextDomain' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s pure-u-1-4 four columns">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'themeHandle_widgets_init' );
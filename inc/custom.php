<?php

if ( ! function_exists( 'themeHandle_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 * Create your own themeTextDomain_entry_meta() to override in a child theme.
 *
 * @method themeHandle_entry_meta
 * @since themeName themeVersion
 *
 * @return {Html}
 */
function themeHandle_entry_meta() {

	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'themeTextDomain' ) . '</span>';

	if ( ! has_post_format( 'aside' ) && ! has_post_format( 'link' ) && 'post' == get_post_type() )
		themeTextDomain_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'themeTextDomain' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'themeTextDomain' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'themeTextDomain' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'themeHandle_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 * Create your own themeTextDomain_entry_date() to override in a child theme.
 *
 * @method themeHandle_entry_date
 * @since themeName themeVersion
 *
 * @param {Boolean} [$echo=true] Whether to echo the date. Default true.
 * @return {Html | String}
 */
function themeHandle_entry_date( $echo = true ) {
	$format_prefix = ( has_post_format( 'chat' ) || has_post_format( 'status' ) ) ? _x( '%1$s on %2$s', '1: post format name. 2: date', 'themeTextDomain' ): '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'themeTextDomain' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

/**
 * Extends the default WordPress body class to denote:
 * 1. Custom fonts enabled.
 * 2. Single or multiple authors.
 * 3. Active widgets in the sidebar to change the layout and spacing.
 *
 * @method themeHandle_body_class
 * @private
 * @type filter
 * @since themeName themeVersion
 *
 * @param {Array} $classes Existing class values.
 * @return {Array} Filtered class values.
 */
function themeHandle_body_class( $classes ) {

	if ( ! is_multi_author() ){
		$classes[] = 'single-author';
	}

	if ( is_active_sidebar( 'sidebar-1' ) && ! is_attachment() && ! is_404() ){
		$classes[] = 'sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'themeHandle_body_class' );


// add_filter( 'post_link', 'future_permalink', 10, 3 );
// add_filter( 'post_type_link', 'future_permalink', 10, 4 );

/**
 * Pretty permalinks for future posts and custom post types
 *
 * @method future_permalink
 * @public
 * @type filter
 * @since themeName themeVersion
 *
 * @param {String} $permalink Existing permalink.
 * @param {WP_Post} $post Post object.
 * @param {Boolean} $leavename
 * @param {Boolean} $sample
 * @return {String} Pretty permalink.
 */
function future_permalink( $permalink, $post, $leavename, $sample = false ) {
	/* for filter recursion (infinite loop) */
	static $recursing = false;

	if ( empty( $post->ID ) ) {
		return $permalink;
	}

	if ( !$recursing ) {
		if ( isset( $post->post_status ) && ( 'future' === $post->post_status ) ) {
			// set the post status to publish to get the 'publish' permalink
			$post->post_status = 'publish';
			$recursing = true;
			return get_permalink( $post, $leavename ) ;
		}
	}

	$recursing = false;
	return $permalink;
}
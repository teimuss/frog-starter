/**
 * Process ajax request for a encyclopedia view
 */
(function($){
  'use strict';

  $( document ).ready(function() {

    $(document).on('click', '.show-more-media', function(ev){
      var options;

      if(PaginationAjax.handlebars===false){
        return;
      }

      options = {
        action: 'media_show',
        type: 'json',
        page: $(this).data('page'),
        handlebars: true,
        container: '#content',
        source: '#media-single-template'
      };

      ev.preventDefault();

      $('.more-media').html('<span class="loading-more-media animate-spin icon-loading-spin"></span>');
      loadAjax($, options, true);
    }); //end encyclopedia-view on click

    var loadAjax = function($, options){
      $.ajax({
        url: PaginationAjax.ajaxurl,
        data: {
          'action': options.action,
          'id' : options.id,
          'page': options.page,
          'nonce' : PaginationAjax.nonce,
          //'lang' : PaginationAjax.lang,
          'type' : 'json' //added after
        },
        type: 'post',
        dataType: options.type,
        context: document.body,
        error: function(xhr,status,error){
          alert(error);
        }
      }).done(function( response ) {
          var source, template, data;

          data = response;
          // CURRENT_MEMBER_ID = data.id;  // store for future refreshes

          if(options.handlebars){
            source = $(options.source).html();
            template = Handlebars.compile(source);


            if (data.navigation.current < data.navigation.total) {
              $('.more-media').html('<span class="show-more-media" data-page="'+data.navigation.next+'">show more</span>');
            }else{
              $('.more-media').remove();
            }

            if($('.more-media').length > 0){
              $('.more-media').before(template(data.items));
            }else{
              $(options.container).append(template(data.items));
            }


          }
          else{
            $(options.container).html(data);
          }

      });
    };

  });

}(jQuery));

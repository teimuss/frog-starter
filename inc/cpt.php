<?php

require_once( 'vendor/jjgrainger/wp-custom-post-type-class/src/CPT.php' );


/**
* Custom Post Type Class
*
* @since themeHandle themeVersion
* @url https://github.com/jjgrainger/wp-custom-post-type-class/
*
*/

// Check if user has admin privileges
//  $show_ui = current_user_can( 'manage_options' ) ? true : false;

//Example Music Custom Post Type
// $names = array(
//   'post_type_name' => 'audio',
//   'singular' => _('Audio'),
//   'plural' => _('Audio'),
//   'slug' => _('audio')
// );
//
// $audio = new CPT($names, array(
//   'supports' => array('title', 'editor', 'thumbnail'),
//   'show_in_menu' => 'upload.php',
//   'has_archive' => true
// ));
//
// $audio->register_taxonomy(array(
//     'taxonomy_name' => _('genre'),
//     'singular' => _('Genre'),
//     'plural' => _('Genres'),
//     'slug' => _('genre')
// ));
//
// $audio->filters(array('genre'));
//
// $audio->set_textdomain('void');
//
// $audio->menu_icon("dashicons-playlist-audio");



?>

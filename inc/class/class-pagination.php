<?php
/**
 * Pagination generation class
 *
 * @module includes
 * @submodule pagination
 */


/**
* Pagination class
*
* @class Pagination
* @static
*/
class Pagination {

  /**
  * @property $initialized
  * @type Boolean
  * @default false
  * @static
  * @private
  */
  private static $initialized = false;

  /**
  * Initialize class actions and filters
  *
  * @method init
  * @static
  */
  public static function init(){
    if (self::$initialized)
      return;

    self::hooks();
    self::enqueue(); //pass to main enqueue
    self::localize();

    self::$initialized = true;
  }


  /**
  * Action and filter hooks
  *
  * @method hooks
  * @private
  * @static
  */
  private static function hooks(){
    add_action('wp_ajax_media_show', array('Pagination', 'action_send_more_media'));
    add_action('wp_ajax_nopriv_media_show', array('Pagination', 'action_send_more_media'));
  }

  /**
  * Enqueue class js and css files
  *
  * @method enqueue
  * @private
  * @static
  * @param {String} [$handler='themeTextDomain-ajax-request'] Handler for the scripts
  */
  private static function enqueue($handler='themeTextDomain-request'){
    wp_enqueue_script(
      $handler,
      get_template_directory_uri() . '/inc/js/pagination.js',
      array( 'jquery' )
    );
  }

  /**
  * Send some parameter to ajax call
  *
  * @method localize
  * @private
  * @static
  * @param {String} [$handler='themeTextDomain-ajax-request'] Handler for the scripts
  */
  private static function localize($handler='themeTextDomain-ajax-request'){
    wp_localize_script( $handler, 'PaginationAjax', array(
    	'ajaxurl' => admin_url( 'admin-ajax.php' ),
    	'nonce' => wp_create_nonce( 'themeTextDomain-ajax-media-nonce' ),
    	// 'lang' => ICL_LANGUAGE_CODE,
    	'home' => home_url(),
    	'handlebars' => true //define in settings.php
    	)
    );
  }

  /**
  * Used by wp_ajax_media_show action (wp_ajax_)
  *
  * @method action_send_more_media
  * @return {Json} Result of data
  */
  public static function action_send_more_media() {
      $options = array(
  			'nonce'			=> '',
  			'action'		=> '',
  			'id'			=> '',
  			'lang'			=> '',
  			'page'			=> ''
  		);

  		// load post options
  		$options = wp_parse_args($_POST, $options);

      // verify nonce
  		if( ! wp_verify_nonce($options['nonce'], 'themeTextDomain-ajax-media-nonce') ) {
  			die(0);
  		}

      $args = array(
        'post_type' => 'audio',
        'paged' => $options['page']
      );

      // The Query
      $the_query = new WP_Query( $args );

      $result = array();

      // The Loop
      $c = 0;
      if ( $the_query->have_posts() ) :
      	while ( $the_query->have_posts() ) : $the_query->the_post();
          $post_id = get_the_ID();
          $thumb_id = get_post_thumbnail_id($post_id);
      		$results['items'][$c]['id'] = $post_id;
      		$results['items'][$c]['classes'] = implode(' ', get_post_class());
      		$results['items'][$c]['title'] = get_the_title();
      		$results['items'][$c]['permalink'] = get_the_permalink();
      		$results['items'][$c]['meta'] = get_the_category_list();
      		$results['items'][$c]['thumbs'] = get_the_post_thumbnail();
      		$results['items'][$c]['has_thumbnails'] = has_post_thumbnail();
      		$results['items'][$c]['soundcloud'] = self::get_type_media($post_id);
      		$results['items'][$c]['images']['xsmall'] = wp_get_attachment_image_src($thumb_id, 'xsmall')[0];
      		$results['items'][$c]['images']['small'] = wp_get_attachment_image_src($thumb_id, 'small')[0];
      		$results['items'][$c]['images']['medium'] = wp_get_attachment_image_src($thumb_id, 'medium')[0];
      		$results['items'][$c]['images']['large'] = wp_get_attachment_image_src($thumb_id, 'large')[0];
      		$results['items'][$c]['images']['xlarge'] = wp_get_attachment_image_src($thumb_id, 'xlarge')[0];
          // $results['items'][$c]['img'] = get_first_image_url(get_the_ID());
          $c++;
      	endwhile;

        // print_r($the_query);

        $current = $the_query->get('paged');
        $total = $the_query->max_num_pages;

        $results['navigation']['next'] = self::get_next_posts_page_number($current, $total);
        $results['navigation']['total'] = $total;
        $results['navigation']['current'] = $current;

      else:
      	// no posts found
      endif;

      wp_send_json($results);

      die();
  }

  /**
  * Send some parameter to ajax call
  *
  * @method get_type_media
  * @private
  * @param {String} $id Post id
  */
  private function get_type_media($id){
      $type = get_field('type', $id);

      if ($type == 'Soundcloud') {
        return true;
      }

      return false;
  }

  /**
  * Displays a more media button if there is more to show
  *
  * @method the_more_media_button
  * @static
  * @return {Html} More media button
  */
  public static function the_more_media_button(){?>
    <?php
    global $wp_query;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 || !self::get_next_posts_page_number()){
      return;
    }
    ?>
    <div class="more-media">
      <span class="show-more-media" data-page="<?php echo self::get_next_posts_page_number(); ?>">show more</span>
    </div>

  <?php
  }

  /**
  * Get the highest page number
  *
  * @method get_max_page_number
  * @static
  * @return {String} Page number
  */
  public static function get_max_page_number(){
    global $wp_query;

    return $wp_query->max_num_pages;
  }

  /**
  * Get the highest page number
  *
  * @method get_next_posts_page_number
  * @static
  * @param {Int | String} [$current = null] Current page number
  * @param {Int | String} [$max_page = null] Number of max page
  * @return {Int | String} Next post page number
  */
  static function get_next_posts_page_number($current=null, $max_page=null) {
    global $paged, $wp_query;


    if (!$max_page) {
      $max_page = $wp_query->max_num_pages;
    }

    if (!$current) {
      $current = $paged;
    }

    if ( !is_single() ) {
      if ( !$current ){
        $current = 1;
      }

      $nextpage = intval($current) + 1;

      if ( !$max_page || $max_page >= $nextpage ){
        return $nextpage;
      }
    }

    return null;
  }


  /**
  *  Displays navigation to next/previous set of posts when applicable.
  *
  * @method the_links_by_page
  * @static
  * @return {Html} Next/previous numbers
  */
  public static function the_links_by_page(){
    global $wp_query;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
      return;
    ?>
    <nav class="navigation paging-navigation" role="navigation">
      <h1 class="assistive-text"><?php _e( 'Posts navigation', 'frogstarter' ); ?></h1>
      <div class="nav-links">

        <?php if ( get_next_posts_link() ) : ?>
        <?php $next_markup = '<div class="nav-previous">'.next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'frogstarter' ) ).'</div>'; ?>
        <?php echo apply_filters('more_pages_to_show', $next_markup, 2); ?>
        <?php endif; ?>

        <?php if ( get_previous_posts_link() ) : ?>
          <?php $previous_markup = '<div class="nav-next">'.previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'frogstarter' ) ).'</div>' ?>
          <?php echo apply_filters('less_pages_to_show', $previous_markup, 1); ?>
        <?php endif; ?>
      </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
  }

 /**
 *  Displays navigation to next/previous title set of posts when applicable
 *
 * @method the_links_by_title
 * @static
 * @return {Html} Next/previous post titles
 */
  public static function the_links_by_title(){
    global $post;

    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
    $next = get_adjacent_post( false, '', false );

    if ( ! $next && ! $previous )
      return;
    ?>
    <nav class="navigation post-navigation" role="navigation">
      <h1 class="assistive-text"><?php _e( 'Post navigation', 'frogstarter' ); ?></h1>
      <div class="nav-links">

        <?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'frogstarter' ) ); ?>
        <?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'frogstarter' ) ); ?>

      </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
  }

}

//Initialize class in inti action
function initialize_pagination(){
  Pagination::init();
}

add_action( 'init', 'initialize_pagination' );

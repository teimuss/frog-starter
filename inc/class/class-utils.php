<?php

/**
 * Note: Waiting to be able to use anonymous functions in the future.
 * @see http://labs.froginredtie.co.uk/wordpress/blog/2015/09/14/code-snippets-class-methods-for-wp-hooker-functions/
 */

/**
 * Set of utility functions
 *
 * @module includes
 * @submodule utility
 */

 /**
 * Set of utility functions
 *
 * @class Util
 * @static
 * @beta
 */

 // Create a base clase???
 //Consider moving some functionality to other classes, such as registration

 class Util {
   public static $active = array();

   public static function init(){
     if (current_theme_supports('full-registration')) {
       self::full_registration();
      //  self::frontend_logout();
       self::frontend_login_fail();
       self::frontend_blank_login();
       self::update_password();
       self::$active = wp_parse_args(['full_registration', 'frontend_logout', 'frontend_login_fail', 'frontend_blank_login', 'update_password'], self::$active);
     }else if(current_theme_supports('basic-registration')){
       self::basic_registration();
       self::frontend_logout();
       self::frontend_login_fail();
       self::frontend_blank_login();
       self::update_password();
       self::$active = wp_parse_args(['basic_registration', 'frontend_logout', 'frontend_login_fail', 'frontend_blank_login', 'update_password'], self::$active);
     }
     if (current_theme_supports('admin-login-page')) {
       self::admin_login_logo();
       self::login_logo_url();
       self::login_logo_title();
       self::$active = wp_parse_args(['admin_login_logo', 'login_logo_url', 'login_logo_title'], self::$active);
     }
   }

   public static function is_active($method){
     return in_array(self::$active, array($method));
   }

   /**
   * Activate certain functionality
   *
   * @method activate
   * @param {String} $method Functionality/Method name
   * @static
   *
   * @return {Boolean | String | Object} Return false in case method does not exist
   */

   public static function activate($method, $args=null){
      if (is_callable(array(__CLASS__, $method))) {
        self::$active[] = $method;
        return self::$method($args);
      }else{
        return false;
      }
   }

   /**
   * List of available functionality to be activated
   *
   * @method list_methods
   * @static
   * @uses ReflectionClass
   */

   public static function list_services(){

    $class = new ReflectionClass('Util');
    $methods = $class->getMethods(ReflectionMethod::IS_PRIVATE);
    echo count($methods).' methods found.';
    self::display_formatted($methods);

   }


   /**
   * Verify date
   *
   * @method verify_date
   * @private
   * @static
   * @uses DateTime
   * @param {String} $date Date
   * @param {String} [$format='m/d/Y'] Format to compare
   *
   * @return {Boolean} Is date in right format?
   */
   private static function verify_date($date, $format='m/d/Y'){
     return (DateTime::createFromFormat($format, $date) !== false);
   }

   /**
   * Disable admin bar
   *
   * @method disable_admin_bar
   * @private
   * @static
   * @param {Boolean} $admin Disable for admin as well
   *
   */
   private static function disable_admin_bar($admin=false){
     if ($admin) {
       show_admin_bar(false);
     }else{
       if (!current_user_can('administrator')):
        show_admin_bar(false);
       endif;
     }
   }


   /**
   * Display formatted array
   *
   * @method display_formatted
   * @public
   * @static
   * @param {Array} $array Data array
   */
   public static function display_formatted($array){
     echo '<pre>';
   	 print_r($array);
   	 echo '</pre>';
   }

   /**
   * Process password update
   * Uses template_redirect action.
   *
   * @method action_update_password
   * @private
   * @type action
   * @static
   */
   public static function action_update_password(){
     if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST) && isset($_POST['update_user_pass'])) :
       unset($_POST['update_user_pass']);

       $user = get_current_user();

       if ($user && !wp_check_password($_POST['old_pass'], $user->user_pass, $user->ID)) :
         wp_redirect( $_SERVER['REQUEST_URI'].'?error=1' );
         exit();
       else:
         if ($_POST['new_pass'] && $_POST['new_pass_repeat'] && $_POST['new_pass'] === $_POST['new_pass_repeat']) :
           wp_set_password($_POST['new_pass'], $user->ID);
           wp_redirect( $_SERVER['REQUEST_URI'].'?update=1' );
           exit();
         else:
           wp_redirect( $_SERVER['REQUEST_URI'].'?error=2' );
           exit();
         endif;
       endif;

     endif;
   }

   /**
   * Process password update
   * Uses template_redirect action.
   *
   * @method update_password
   * @private
   * @static
   */
   private static function update_password() {
     add_action( 'template_redirect', array('Util', 'action_update_password') );
   }

   /**
   * Process and create a user from front-end
   * Uses template_redirect action.
   *
   * @method action_full_registration
   * @private
   * @type action
   * @static
   */
   public static function action_full_registration(){
     if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST) && isset($_POST['full_registration'])) :
       unset($_POST['full_registration']);

       $userdata = array();

       foreach ($_POST as $key => $value) {
         $userdata[$key] = trim($value);
       }

       $userdata['user_login'] = $_POST['user_email'];

       if (email_exists($userdata['user_email'])) :
         wp_redirect( $_SERVER['REQUEST_URI'].'?signup=email_exists' );
         exit();
       else:
         $user = wp_insert_user($userdata);


         if (is_wp_error($user)) :
           $error_string = $user->get_error_message();
           wp_redirect( $_SERVER['REQUEST_URI'].'?signup=failed' );
           exit();
         else:
           do_action('evb_sign_up_successfuly');
           do_action('evb_send_activation_email', $user);
            //wp_new_user_notification($user,null, 'both');
           wp_redirect( $_SERVER['REQUEST_URI'].'?signup=success' );
         endif;
       endif;
     endif;
   }

   /**
   * Process and create a user from front-end
   * Uses template_redirect action.
   *
   * @method full_registration
   * @private
   * @static
   */
   private static function full_registration() {
     add_action( 'template_redirect', array('Util', 'action_full_registration') );
   }

   /**
   * Process and create a user with only an email
   * Uses template_redirect action.
   *
   * @method action_basic_registration
   * @private
   * @type action
   * @static
   */
   public static function action_basic_registration(){
     if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST) && isset($_POST['basic_registration'])) :
       $email = $_POST['email_address'];

       if (email_exists($email) || username_exists($email)) :
         wp_redirect( $_SERVER['REQUEST_URI'].'?error=1' );
         exit();
       else:
         $password = wp_generate_password(8);
         $user = wp_create_user($email, $password, $email);
         if (is_wp_error($user)) :
           wp_redirect( $_SERVER['REQUEST_URI'].'?error=1' );
           exit();
         else:
           if ($_POST['is_promoter'] === 'promoter') :
             $user_role = new WP_User($user);
             $user_role->set_role('promoter');
           endif;
            // triggers deprecated function argument is deprecated
            //  wp_new_user_notification($user, $password);
            do_action('evb_send_activation_email', $user);
         endif;
       endif;
     endif;
  }

 /**
 * Process and create a user with only an email
 * Uses template_redirect action.
 *
 * @method basic_registration
 * @private
 * @static
 */
 private static function basic_registration() {
   add_action( 'template_redirect', array('Util', 'action_basic_registration') );
 }

   /**
  * Prevent logout redirect to admin logout page.
  * Logout stays in the same page.
  * Uses wp_logout action.
  *
  * @method action_frontend_logout
  * @private
  * @type action
  * @static
  */
  public static function action_frontend_logout(){
    if (isset($_SERVER['HTTP_REFERER'])) {
      $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    }
    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?logout=true' );
      exit;
    }
  }


   /**
   * Prevent logout redirect to admin logout page.
   * Logout stays in the same page.
   * Uses wp_logout action.
   *
   * @method frontend_logout
   * @private
   * @static
   */
   private static function frontend_logout() {
     add_action( 'wp_logout', array('Util', 'action_frontend_logout') );
   }

   /**
   * Prevent redirect to admin login page.
   * Login stays in the same page.
   * Uses wp_login_failed action.
   *
   * @method action_frontend_login_fail
   * @private
   * @static
   * @type action
   */
   public static function action_frontend_login_fail() {
      if (isset($_SERVER['HTTP_REFERER'])) {
        $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
      }
      // if there's a valid referrer, and it's not the default log-in screen
      if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
         if (!strstr($referrer, '?login=failed')) {
           wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
           exit;
         }else{
           wp_redirect($referrer);
           exit;
         }
      }
   }

   /**
   * Prevent redirect to admin login page.
   * Login stays in the same page.
   * Uses wp_login_failed action.
   *
   * @method frontend_login_fail
   * @private
   * @static
   * @uses wp_login_failed action
   */
   private static function frontend_login_fail(){
     add_action( 'wp_login_failed', array('Util', 'action_frontend_login_fail') );
   }

  /**
  * Prevent redirect to admin login page when user uses blank logins.
  * Login stays in the same page.
  * Uses wp_authenticate action.
  *
  * @method action_frontend_blank_login
  * @private
  * @static
  * @type action
  */
  public static function action_frontend_blank_login($username) {
     if (isset($_SERVER['HTTP_REFERER'])) {
       $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
     }

     $error = false;
     if (empty($username) || empty($_POST['pwd'])) {
       $error = true;
     }


     // if there's a valid referrer, and it's not the default log-in screen
     if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $error) {
        if (!strstr($referrer, '?login=failed')) {
          wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
          exit;
        }else{
          wp_redirect($referrer);
          exit;
        }
     }
  }

  /**
  * Prevent redirect to admin login page.
  * Login stays in the same page.
  * Uses wp_login_failed action.
  *
  * @method frontend_login_fail
  * @private
  * @static
  * @uses wp_authenticate action
  */
  private static function frontend_blank_login(){
    add_action( 'wp_authenticate', array('Util', 'action_frontend_blank_login') );
  }


   /**
   * Assigns a class of firts-menu-item and last-menu-item to the first and last item of the menu respectively.
   * Uses wp_nav_menu action.
   *
   * @method filter_add_markup_to_menu
   * @private
   * @type filter
   * @static
   */
   public static function filter_add_markup_to_menu($output) {
     $output= preg_replace('/menu-item/', 'first-menu-item menu-item', $output, 1);
     $output=substr_replace($output, "last-menu-item menu-item", strripos($output, "menu-item"), strlen("menu-item"));
     return $output;
   }

   /**
   * Assigns a class of firts-menu-item and last-menu-item to the first and last item of the menu respectively.
   * Uses wp_nav_menu action.
   *
   * @method add_markup_to_menu
   * @private
   * @static
   * @uses wp_nav_menu filter
   */
   private static function add_markup_to_menu(){
     add_filter('wp_nav_menu', array('Util', 'filter_add_markup_to_menu'));
   }

   /**
   * Adds a logo for the WordPress admin login page
   *
   * @method action_admin_login_logo
   * @private
   * @type action
   * @static
   */
    public static function action_admin_login_logo(){
      echo '<style  type="text/css"> h1 a {  background-image:url(' . get_template_directory_uri() . '/images/logo_buzz_black.png)  !important;} </style>';
    }

    /**
   * Adds a logo for the WordPress admin login page
   *
   * @method admin_login_logo
   * @private
   * @static
   * @uses login_head action
   */
    private static function admin_login_logo(){
      add_action('login_head',  array('Util', 'action_admin_login_logo'));
    }

    /**
    * Change default url of admin login logo
    *
    * @method filter_login_logo_url
    * @private
    * @type filter
    * @static
    */
     public static function filter_login_logo_url(){
       return get_bloginfo('url');
     }

     /**
    * Change default url of admin login logo
    *
    * @method login_logo_url
    * @private
    * @static
    * @uses login_head_url_title filter
    */
     private static function login_logo_url(){
       add_filter('login_headerurl',  array('Util', 'filter_login_logo_url'));
     }

     /**
     * Change default title of admin login logo
     *
     * @method filter_login_logo_title
     * @private
     * @type filter
     * @static
     */
      public static function filter_login_logo_title(){
        return get_bloginfo('title');
      }

      /**
     * Change default title of admin login logo
     *
     * @method login_url_title
     * @private
     * @static
     * @uses login_head_url_title filter
     */
      private static function login_logo_title(){
        add_filter('login_headertitle',  array('Util', 'filter_login_logo_title'));
      }

 }

 //Initialize class in inti action
 function initialize_util_lib(){
   Util::init();
 }

 add_action( 'init', 'initialize_util_lib' );

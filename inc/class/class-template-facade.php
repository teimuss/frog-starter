<?php

  /**
  * Encapsulates some template logic in order to simplify template files.
  *
  * @module includes
  * @submodule template
  */

  /**
  * Encapsulates some template logic in order to simplify template files.
  *
  * @class TemplateFacade
  * @static
  * @main
  */
  class TemplateFacade {

  }


  /**
   * Logic for events templates
   * @class TemplateEvents
   * @extends TemplateFacade
   * @example This is an example 
   */
  class TemplateEvents extends TemplateFacade{

    /**
    * Get event date from ACF field
    * @method  get_event_date
    * @static
    * @param {String} $field ACF field name
    * @uses ACF class
    */
    public static function get_event_date($field){
      $object = get_field_object($field);
      $format = $object['return_format'];
      $date = DateTime::createFromFormat($format, get_field($field));

      return $date;
    }


  }



 ?>

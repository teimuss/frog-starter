# Routing system

This set pf classes help you interject code into pages and posts.
To acommplish that it uses a routing system which indentifies ur paths
with pre-defined routes.

### Update loader

Update psr-4 loaders
```
composer dump-autoload --optimize
```


### Use example

Enquue scripts for specific pages
```php
use FIRT\RouterMiddleware;

$router = new FIRT\RouterMiddleware();
$router->init();

$router->addFrontendRoute('/', function () {
    wp_enqueue_style('tests-main', get_template_directory_uri() . '/dist/styles/main.css', false, 'c9f2155733c68254f446eb06faa23581');
});

$router->addFrontendRoute('who-we-are', function () {
    echo 'This is who we are page';
});
```

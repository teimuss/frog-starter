<?php
/**
 * Abstract class for all routes
 *
 * @package FIRT
 * @subpackage Routing
 * @since 0.1.0
 */

namespace FIRT;

/**
 * Abstract class for all routes.
 *
 * @since 0.1.0
 */
abstract class ARouter implements IRouter {
	/**
	 * Add route filter's' name.
	 *
	 * @since 0.1.0
	 * @access private
	 * @var string $filter_name Add route filter's' name.
	 */
	private $filter_name = 'routing_add_routes';
	/**
	 * Initialization method.
	 *
	 * @since 0.1.0
	 * @access public
	 */
	public function init() {
		// init.
	}

	/**
	 * Return the current url
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @return string Current url.
	 */
	public function get_current_url() {
		$current_url = trim( esc_url_raw( add_query_arg( [] ) ), '/' );
		$home_path = trim( wp_parse_url( home_url(), PHP_URL_PATH ), '/' );

		if ( $home_path && strpos( $current_url, $home_path ) === 0 ) {
			$current_url = trim( substr( $current_url, strlen( $home_path ) ), '/' );
		}

		return $current_url;
	}

	/**
	 * Add routes.
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @param  string   $pattern Url string patern.
	 * @param  callable $callback function to inject code.
	 */
	public function add_frontend_route( $pattern, callable $callback ) {
		add_filter( $this->filter_name, function ( $routes ) use ( $pattern, $callback ) {
			$routes[ $pattern ] = $callback;
			return $routes;
		});
	}

	/**
	 * The filter name for add routes.
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @return string The filter name.
	 */
	public function get_filter_name() {
		return $this->filter_name;
	}

	/**
	 * Set the filters name.
	 *
	 * @since 0.1.0
	 *
	 * @param string $name Name of the filter.
	 */
	public function set_filter_name( $name ) {
		$this->filter_name = $name ? $name : $this->filter_name ;
	}

	/**
	 * Try to match routes with url
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @global WP $wp Current WordPress environment instance.
	 *
	 * @param bool $do_parse Whether or not to parse the request. Default true.
	 * @param WP   $wp Current WordPress environment instance.
	 * @return bool Whether or not to parse the request. Default true.
	 */
	public function parse_routes( $do_parse, $wp ) {
		$allowed = ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX );

		$routes = [];
		$current_url = $this->get_curren_url();

		/**
		 * Filters routing add routes.
		 *
		 * @since 0.1.0
		 *
		 * @param array  $routes Routes array.
		 * @param string $current_url The current url.
		 * @return array $routes Routes array.
		 */
		$routes = apply_filters( $this->filter_name, $routes, $current_url );

		if ( empty( $routes ) || ! is_array( $routes ) || ! $allowed ) {
			return $do_parse;
		}

		$url_parts = explode( '?', $current_url, 2 );
		$url_path = trim( $url_parts[0], '/' );
		$url_vars = [];

		if ( isset( $url_parts[1] ) ) {
			parse_str( $url_parts[1], $url_vars );
		}

		$query_vars = null;

		foreach ( $routes as $pattern => $callback ) {
			if ( preg_match( '~' . trim( $pattern, '/' ) . '~', $url_path, $matches ) ) {
				$route_vars = $callback($matches);
				if ( is_array( $route_vars ) ) {
					$query_vars = array_merge( $route_vars, $url_vars );
					break;
				}
			}
		}

		if ( is_array( $query_vars ) ) {
			$wp->query_vars = $query_vars;
			/**
			 * Has query vars .
			 *
			 * @since 0.1.0
			 *
			 * @param array $query_vars  Query vars array.
			 */
			do_action( 'routing_matched_vars', $query_vars );
			return false;
		}

		return $do_parse;
	}
}

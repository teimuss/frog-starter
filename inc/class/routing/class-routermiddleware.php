<?php
/**
 * Closure middleware for routes
 *
 * This class allow to run a closure on the matched route.
 *
 * @package FIRT
 * @subpackage Routing
 * @since 0.1.0
 */

namespace FIRT;

/**
 * Closure middleware for routes.
 *
 * This class allow to run a closure on the matched route.
 *
 * @since 0.1.0
 */
class RouterMiddleware extends ARouter {
	/**
	 * Class constructor.
	 *
	 * @since 0.1.0
	 */
	public function __construct() {
		parent::set_filter_name( 'routing_add_middleware_routes' );
	}

	/**
	 * Initialization method.
	 *
	 * @since 0.1.0
	 * @access public
	 */
	public function init() {
		add_action( 'do_parse_request', array( $this, 'parse_routes' ), 2 );
	}

	/**
	 * Try to match routes with url
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @global WP $wp Current WordPress environment instance.
	 *
	 * @param bool $do_parse Whether or not to parse the request. Default true.
	 * @param WP   $wp Current WordPress environment instance.
	 * @return bool Whether or not to parse the request. Default true.
	 */
	public function parse_routes( $do_parse, $wp ) {

		$allowed = ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX );

		$routes = [];
		$current_url = $this->get_current_url();

		/**
		 * Filters routing add routes.
		 *
		 * @since 0.1.0
		 *
		 * @param array  $routes Routes array.
		 * @param string $current_url The current url.
		 * @return array $routes Routes array.
		 */
		$routes = apply_filters( parent::get_filter_name(), $routes, $current_url );

		if ( empty( $routes ) || ! is_array( $routes ) || ! $allowed ) {
			return $do_parse;
		}

		$url_parts = explode( '?', $current_url, 2 );
		$url_path = trim( $url_parts[0], '/' );
		$url_vars = [];

		if ( isset( $url_parts[1] ) ) {
			parse_str( $url_parts[1], $url_vars );
		}

		$query_vars = null;

		foreach ( $routes as $pattern => $callback ) {
			if ( preg_match( '~' . trim( $pattern, '/' ) . '~', $url_path, $matches ) ) {
				$route_vars = $callback($matches);
			}
		}

		return $do_parse;
	}
}

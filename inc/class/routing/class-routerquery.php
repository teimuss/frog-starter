<?php
/**
 * Query middleware for routes
 *
 * This class allow to change WordPress main query.
 *
 * @package FIRT
 * @subpackage Routing
 * @since 0.1.0
 */

namespace FIRT;

/**
 * Query middleware for routes.
 *
 * This class allow to change WordPress main query.
 *
 * @since 0.1.0
 */
class RouterQuery extends ARouter {
	/**
	 * Class constructor.
	 *
	 * @since 0.1.0
	 */
	public function __construct() {
		// construct function.
	}
}

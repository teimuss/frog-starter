<?php
/**
 * Interface for all router classes
 *
 * @package FIRT
 * @subpackage Routing
 * @since 0.1.0
 */

namespace FIRT;

/**
 * Interface for all router classes
 *
 * @since 0.1.0
 */
interface IRouter {
	/**
	 * Initialization method.
	 *
	 * @since 0.1.0
	 * @access public
	 */
	public function init();
	/**
	 * Return the current url
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @return string Current url.
	 */
	public function get_current_url();
	/**
	 * Add routes.
	 *
	 * @since 0.1.0
	 * @access public
	 *
	 * @param  string   $pattern Url string patern.
	 * @param  callable $callback function to inject code.
	 */
	public function add_frontend_route( $pattern, callable $callback );
}

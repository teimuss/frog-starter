<?php
/**
 * Image generation and attachment class
 *
 * @module includes
 * @submodule image-responsive
 */


/**
* Image responsive class
*
* @class ImageFill
* @static
*/
class ImageFill{
  /**
  * @property $set_sizes
  * @type Boolean
  * @default false
  * @static
  * @private
  */
  private static $set_sizes = false;

  /**
  * @property $initialized
  * @type Boolean
  * @default false
  * @static
  * @private
  */
  private static $initialized = false;

  /**
  * Initialize class actions and filters
  *
  * @method init
  * @static
  */
  public static function init(){
    if (self::$initialized)
  		return;

    add_action('after_switch_theme', array('ImageFill', 'action_default_theme_sizes'));
    if (current_theme_supports('oembed-2-image')) {
      add_filter('oembed_dataparse', array('ImageFill', 'filter_create_attachment'),10,3);
    }

    self::$initialized = true;
  }

  /**
  * Used by oembed_dataparse filter
  *
  * @method filter_create_attachment
  * @param {String} $return The returned oEmbed HTML
  * @param {Object} $data A data object result from an oEmbed provider
  * @param {String} $url The URL of the content to be embedded
  * @type filter
  * @private
  * @return {String} oEmbed HTML
  */
  public function filter_create_attachment($return, $data, $url) {

      if ($data && is_admin() && !has_post_thumbnail()){
        self::attach_media($data);
      }

  		return $return;
  }

  /**
  * Create media attachment from an oEmbed provider data.
  * Do not create duplicate.
  *
  * @method attach_media
  * @static
  * @param {Object} $data A data object result from an oEmbed provider
  *
  * @return {String|WP_Error} Populated HTML img tag on success, WP_Error object otherwise.
  */
  public static function attach_media($data){
  	if ($data && is_admin()) {

  		if ($data->provider_name == 'YouTube') {
  			preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $data->thumbnail_url, $matches );
  			$name = basename(dirname($matches[0]));
  		}else{
  			$name = ImageFill::get_fileName($data->thumbnail_url);
  		}

  		if (!post_exists($name)) {
  			return media_sideload_image($data->thumbnail_url, get_the_ID(), $name);
  		}

  	}
  }

  /**
  * Get the file name from url
  *
  * @method get_fileName
  * @private
  * @static
  * @param {String} $file File url
  * @param {Boolean} [$ext=false] Returns file name with or without extension
  *
  * @return {String} File name with or without extension.
  */
  private static function get_fileName($file, $ext=false){
  	if ( ! empty( $file ) ) {
      // Set variables for storage, fix file filename for query strings.
      preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches );

      if ($ext) {
  			$name = basename( $matches[0] );
      }else{
  			$name = preg_replace('/\.[^.]+$/', '', basename($file));
  		}

  	}

  	return $name;
  }

  /**
  * Used by after_switch_theme action
  *
  * @method action_default_theme_sizes
  * @private
  * @type action
  */
  public static function action_default_theme_sizes(){
    add_image_size('xsmall', 300, 9999);
    add_image_size('small', 568, 9999);
    update_option('medium_size_w', 768);
    update_option('medium_size_h', 9999);
    update_option('large_size_w', 1024);
    update_option('large_size_h', 9999);
    add_image_size('xlarge', 1280, 9999);
    self::$set_sizes = true;
  }

  /**
  * Add image sources sources to go to the picture element
  *
  * @method add_picture_sources
  * @private
  * @static
  * @param {String} $id Attachment Id
  * @return {String} List of source elements to add
  */
  private static function add_picture_sources($id) {
    //$img_xsmall = wp_get_attachment_image_src($id, 'xsmall');
    $img_small = wp_get_attachment_image_src($id, 'small');
    $img_med = wp_get_attachment_image_src($id, 'medium');
    $img_large = wp_get_attachment_image_src($id, 'large');
    $img_xlarge = wp_get_attachment_image_src($id, 'xlarge');

    $srcset =  '<source srcset ="' . $img_xlarge[0] . '" media="(min-width: 80em)">';
    $srcset .= '<source srcset ="' . $img_large[0] . '" media="(min-width: 64em)">';
    $srcset .= '<source srcset ="' . $img_med[0] . '" media="(min-width: 48em)">';
    $srcset .=  '<source srcset ="' . $img_small[0] . '" media="(min-width: 35.5em)">';
    //$srcset .=  '<source srcset ="' . $img_xsmall[0] . '" media="(min-width: 20em)">';

    return $srcset;
  }

  /**
  * Get alt attibute from attachment
  *
  * @method get_alt
  * @private
  * @param {String} $id Attachment Id
  * @static
  * @return {String} Alt text from attachment
  */
  private static function get_alt($id){
    $img_alt = trim( strip_tags( get_post_meta( $id, '_wp_attachment_image_alt', true ) ) );
    return $img_alt;
  }

  /**
  * Render html5 figure element with picture and all its responsive sources
  *
  * @method render
  * @param {String} $id Attachment Id
  * @static
  * @return {String} Return figure element html
  */
  public static function render($id){
    return '<figure>
    <picture>
    <!--[if IE 9]><video style="display: none;"><![endif]-->' .
    self::add_picture_sources($id) .
    '<!--[if IE 9]></video><![endif]-->
    <img srcset="' . wp_get_attachment_image_src($id, 'xsmall')[0] . '" class="wp-post-image lazyload" alt ="' . self::get_alt($id) . '">
    </picture><figcaption class="et_pb_text et_pb_text_align_center">' . self::get_alt($id) . '</figcaption></figure>';
  }

  /**
  * Get first image url from all attachments in the post
  * Checks for featured image, acf gallery or repeater components on that order
  *
  * @method get_first_image
  * @static
  * @param {String} $post_id The post id
  * @param {String} [$field_name=''] The name of the field
  * @param {String} [$sub_field_name=''] The name of the sub field
  * @return {String} First image url
  */
  public static function get_first_image($post_id, $field_name='', $sub_field_mame=''){

   if(has_post_thumbnail($post_id)):
     $thumb_id = get_post_thumbnail_id($post_id);
     $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
     return $thumb_url_array[0];
   endif;

   if(class_exists('acf') || function_exists('acf_register_repeater_field')):
     $field = get_field_object($field_name, $post_id);

     if($field['type'] === 'gallery'): //gallery
       $rows = get_field($field_name, $post_id); // get all the rows

       if(count($rows)>0):
         $first_row = $rows[0]; // get the first row
         return $first_row;
       endif;

     elseif($field['type'] === 'repeater'): //repeater
       $rows = get_field($field_name, $post_id); // get all the rows

       if(count($rows)>0):
         $first_row = $rows[0]; // get the first row
         return $first_row[$sub_field_mame]['url']; // get the sub field value
       endif;

     endif;
   endif;

   return;
  }

}

  //Initialize class in inti action
  function initialize_image_fill(){
    ImageFill::init();
  }

  add_action( 'init', 'initialize_image_fill' );
?>

<?php

add_action( 'admin_menu', 'frogstarter_admin_menu' );

function frogstarter_admin_menu() {
   remove_menu_page('link-manager.php');
   remove_menu_page( 'edit.php' );   
   remove_menu_page( 'edit.php?post_type=cookielawinfo');
   remove_menu_page( 'edit.php?post_type=page');
   remove_menu_page( 'tools.php');
   remove_menu_page( 'plugins.php' );
   remove_menu_page( 'edit-comments.php' );  
   remove_submenu_page('themes.php', 'widgets.php');
   remove_submenu_page('themes.php', 'themes.php');
}

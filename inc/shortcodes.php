<?php
/**
 * Shorcode's code page'
 *
 * Here you can code the various shorcodes you can use in your theme
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeVersion
 */

/**
 * Shortcode example.
 *
 * Application form for a contest example.
 *
 * @since 0.1.0
 *
 * @param array  $atts Shortcode attributes.
 * @param string $content Optional.
 */
function win_shortcode( $atts, $content = null ) {

	$atts = shortcode_atts(
		array(
			'list_number' => '1',
			'button_label' => 'Save',
			'background_image_url' => null,
		),
		$atts
	);

	if ( ! empty( $content ) ) {
		update_option( 'win_' . get_the_ID() . '_text', $content );
	}

	if ( $atts['background_image_url'] ) {
		update_option( 'win_' . get_the_ID() . '_bacground_image_url', $atts['background_image_url'] );
	}

	require locate_template( 'partials/win-form.php' );

}

add_shortcode( 'win', 'win_shortcode' );

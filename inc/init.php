<?php
/**
* Setup some of the theme core functionality.
* Like load_theme_textdomain, add_theme_support, etc.
*
* @method themeHandle__setup
* @type action
* @private
* @uses load_theme_textdomain() For translation/localization support.
* @uses add_editor_style() To add a Visual Editor stylesheet.
* @uses add_theme_support() To add support for automatic feed links, post
* formats, admin bar, and post thumbnails.
* @uses register_nav_menu() To add support for a navigation menu.
* @uses set_post_thumbnail_size() To set a custom post thumbnail size.
*
* @since themeName themeVersion
*
* @return void
*/
function themeHandle_setup() {

 // Make theme available for translations
 load_theme_textdomain( 'themeTextDomain', get_template_directory() . '/languages' );

 // Styling the visual editor
 //add_editor_style( 'css/editor-style.css' );

 // Adds RSS feed links to <head> for posts and comments.
 add_theme_support( 'automatic-feed-links' );

 // Add html5 support
 add_theme_support('html5', array('search-form'));

 // Post thumbnails
 add_theme_support( 'post-thumbnails' );

 // Automatically creates images from oembeds
 //add_theme_support( 'oembed-2-image' );

 // Post format support
 /*
 add_theme_support( 'post-formats', array(
   'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
 ) );
 */

 // Menus registrations
 register_nav_menus(
   array(
     'primary' => __( 'Navigation Menu', 'themeTextDomain'),
     'footer-about' => __( 'About Footer Menu', 'themeTextDomain'),
     'footer-help' => __( 'Help Footer Menu', 'themeTextDomain')
   )
 );

}

add_action( 'after_setup_theme', 'themeHandle_setup' );

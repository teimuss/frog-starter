<?php
/**
 * Tiny functions
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeVersion
 */

setup_tinymce_plugin();


/**
 * Check if the current user can edit Posts or Pages, and is using the Visual Editor.
 *
 * If so, add some filters so we can register our plugin.
 *
 * @since themeVersion
 */
function setup_tinymce_plugin() {

	// Check if the logged in WordPress User can edit Posts or Pages.
	// If not, don't register our TinyMCE plugin.
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
		return;
	}

	// Check if the logged in WordPress User has the Visual Editor enabled.
	// If not, don't register our TinyMCE plugin.
	if ( get_user_option( 'rich_editing' ) !== 'true' ) {
		return;
	}

	// Setup some filters.
	add_filter( 'mce_external_plugins', 'add_tinymce_plugin' );
	add_filter( 'mce_buttons', 'add_tinymce_toolbar_button' );
}


/**
 * Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance.
 *
 * @since themeVersion
 *
 * @param array $plugin_array Array of registered TinyMCE Plugins.
 * @return array Modified array of registered TinyMCE Plugins.
 */
function add_tinymce_plugin( $plugin_array ) {
	$plugin_array['example'] = get_template_directory_uri() . '/inc/js/tinymce/plugins/example/plugin.js';
	return $plugin_array;
}


/**
 * Adds a button to the TinyMCE / Visual Editor which the user can click to insert a link with a custom CSS class.
 *
 * @since 0.1.0
 *
 * @param array $buttons Array of registered TinyMCE Buttons.
 * @return array Modified array of registered TinyMCE Buttons.
 */
function add_tinymce_toolbar_button( $buttons ) {
	array_push( $buttons, '|', 'example' );
	return $buttons;
}


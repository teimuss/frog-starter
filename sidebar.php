<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in this sidebar, it will be hidden completely.
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeName themeVersion
 */

if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
	<div id="secondary" class="sidebar-container" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-main' ); ?>
			</div>
		</div>
	</div><!-- #secondary -->
<?php endif; ?>

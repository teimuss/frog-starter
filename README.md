# themeName - WordPress Theme starter

##Icon font integration
Easy integration with fontawesome and fontello.

### Fontawesome
####Fontawesome install
```
bower --save install fontawesome
```
#### Configuration
* Copy files from bower_components to sass directory.
* Change font-awesome.scss to a partial _font-awesome.scss.
* Import fontawesome into main.scss

```
@import "vendor/fontawesome/font-awesome";
```

### Fontello
Fontello comes as a grunt plugin
* Make sure to have config.json on the project root
* run grunt task:

```
grunt fontello
```
* Import fontello into main.scss

```
@import "vendor/fontello/main";
```

## Ngrok web server
Ngrok server allows to serve your local webpage on the web.
###Usage
* Enter the tools directory on the project root and run the command:

```
./run-ngrok.cmd
```
#### ngrok web client
```
http://localhost:4040
```

#### ngrok client status page
If you have an ngrok client running, just click the 'Status' link in the top navigation of the web interface.
```
http://localhost:4040/status
```

## Bourbon and Neat

Check if bourbon/neat is installed as a gem
```
gem list --local
```

### Install
```
gem install bourbon
```
```
gem install neat
```

* Enter the sass folder and type:

```
bourbon install
```
```
neat install
```

### Usage

```
@import "bourbon/bourbon";
```

## YUIDoc

* copy yuidoc-theme to this project

```
$ grunt yuidoc
```

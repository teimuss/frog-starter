<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage themeHandle
 * @since themeName themeVersion
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'footer' ); ?>

			<div class="site-info">
				<?php do_action( 'themeHandle_credits' ); ?>
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'themeTextDomain' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'themeTextDomain' ); ?>"><?php printf( __( 'Proudly powered by %s', 'themeTextDomain' ), 'WordPress' ); ?></a>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>
